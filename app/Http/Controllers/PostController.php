<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Auth (middleware) - access the authenticated user via Auth Facades.
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;


class PostController extends Controller
{
    // Action to return a view containing a form for a blog post creation
    public function create()
        {
            return view('posts.create');
        }

    public function store(Request $request)
        {
            if(Auth::user()) {
                // instantiate a new Post object from the Post Model
                $post = new Post;

                // define the properties of the $post object using the received form data.
                $post->title = $request->input('title');
                $post->content = $request->input('content');
                // get the id of the authenticated user and set it as foreign key user_id of the new post.
                $post->user_id = (Auth::user()->id);

                // save this post object into the database.
                $post->save();
               

                return redirect('/posts');

            } else {
                return redirect('/login');
            }

        }

    // action that will return a view showing all blog posts.
    public function index()
        {
            $posts = Post::where('isActive', true)->get();
            return view('posts.index')->with('posts', $posts);
        }

    // action for showing only the posts authored by the authenticated user.
    public function myPosts()
        {
            if(Auth::user()) {
                $posts = Auth::user()->posts;
                return view('posts.index')->with('posts', $posts);
            } else {
                return redirect('/login');
            }
        }



    public function welcome()
        {
            $posts = Post::inRandomOrder()->limit(3)->get();
            return view('welcome')->with('posts', $posts);
        }


    // action that will return a view showing a speicific post using the URL parameter $id.
    public function show($id)
        {
            $post = Post::find($id);
            return view('posts.show')->with('post', $post);
        }


    // action that will return a view showing an edit post form for a specific post using the URL parameter $id to query for the database entry to be edited
    public function edit($id)
        {
            $post = Post::find($id);
            return view('posts.edit')->with('post', $post);
        }

    // action to update a specific post by an authenticated user.
    public function update(Request $request, $id)
        {
            $post = Post::find($id);

            if (!$post) {
                return redirect('/posts')->with('error', 'Post not found');
            }

            // If authenticated user's id ID is the same as the post's user_id.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();

            return redirect('/posts/' . $id);
        }


    // define action/function for deleting post
    // public function destroy($id)
    //     {
    //         $post = Post::find($id);

    //         if(Auth::user()->id === $post->user_id) {
    //             $post->delete();
    //         } 
    //         return redirect('/posts');
    //     }
    

    public function archive($id)
            {
                $post = Post::find($id);
                $post->isActive = false;
                $post->save();
                return redirect('/myPosts');
            }


    public function activate($id)
           {
               $post = Post::find($id);
               $post->isActive = true;
               $post->save();
               return redirect('/myPosts');
           }

    public function like($id)
            {
                $post = Post::find($id);
                $user_id = Auth::user()->id;

                // if an authenticated user is not the post author
                if($post->user_id != $user_id) {
                    // this checks if a post like has been made by a certain user before
                    if ($post->likes->contains("user_id", $user_id)) {
                        // delete the like made by this user to unlike this post
                         PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
                    } else {
                        // create a new like record to like a specific post.
                        // instantiate a new PostLike object fromm the PostLike model.
                        $postLike = new PostLike;

                        // define the properties of the $postLike object
                        $postLike->post_id = $post->id;
                        $postLike->user_id = $user_id;

                        // save this $postLike obejct into the database 
                        $postLike->save();

                    }

                    // redirect the user back to the same post.
                    return redirect("/posts/$id");
                }
            }
    public function comment(Request $request, $id)
            {
                $request->validate(['content' => 'required|string',]);

                $post = Post::find($id);
                if (!$post) {
                    abort(404);
                }

                $comment = new PostComment([
                    'content' => $request->input('content'),
                ]);

                $comment->post()->associate($post);
                $comment->user()->associate($request->user());
                $comment->save();

                return redirect()->back();
            }

    

}











