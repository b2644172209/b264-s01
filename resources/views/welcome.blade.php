@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="text-center mb-5">
                    <img src="https://miro.medium.com/max/697/1*uwOcGqshCks5HoSJvEJrrw.png" alt="Laravel Logo">
                </div>
                <h2 class="text-center mb-4">Featured Posts</h2>
                <hr>
                @if(count($posts) > 0)
                    @foreach($posts as $post)
                        <div class="card text-center mb-3">
                            <div class="card-body">
                                <h4 class="card-title"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                                <h6 class="card-text">Author: {{$post->user->name}}</h6>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p>There are no featured posts at the moment.</p>
                @endif
            </div>
        </div>
    </div>
@endsection
