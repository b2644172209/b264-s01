@extends('layouts.app')

@section('content')
    <h1>Edit Post</h1>
    <form method="POST" action="/posts/{{$post->id}}">
        @csrf
        @method('PUT')
        <div class="row mb-3">
            <label class="mb-3" for="title">Title:</label>

            <div class="col-md-6">
              <input type="text" name="title" value="{{ $post->title }}" required>
            </div>
        </div>
        <div class="row mb-3">
            <label class="mb-3" for="content">Content:</label>
            <div>
              <textarea class="col-md-4 col-form-label" name="content" required>{{ $post->content }}</textarea>
            </div>
        </div>

        @if (Auth::user()->id === $post->user_id)
        <div class="mt-2">
        <button type="submit" class="btn btn-primary">Update Post</button>
        </div>
        @endif

    </form>
@endsection





